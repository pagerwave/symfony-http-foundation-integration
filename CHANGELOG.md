# v1.2.0 (2023-01-20)

* Allow PHP 8.2

# v1.1.0 (2021-11-30)

* Allow PHP 8.1
* Drop support for PagerWave 1.x

# v1.0.0 (2020-11-29)

* Split off from main PagerWave repository.
