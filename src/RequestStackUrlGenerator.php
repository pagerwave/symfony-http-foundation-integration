<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony;

use PagerWave\Exception\UrlGenerationFailedException;
use PagerWave\UrlGenerator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final class RequestStackUrlGenerator implements UrlGeneratorInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function generateUrl(string $group, array $params): string
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            throw new UrlGenerationFailedException('No request in RequestStack');
        }

        return (new RequestUrlGenerator($request))
            ->generateUrl($group, $params);
    }
}
