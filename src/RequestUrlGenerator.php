<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony;

use PagerWave\UrlGenerator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;

final class RequestUrlGenerator implements UrlGeneratorInterface
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function generateUrl(string $group, array $params): string
    {
        $query = array_replace($this->request->query->all(), [
            $group => $params,
        ]);

        $uri = Request::create(
            sprintf('%s%s%s',
                $this->request->getBaseUrl(),
                $this->request->getBasePath(),
                $this->request->getPathInfo()
            ),
            'GET',
            [],
            [],
            [],
            $this->request->server->all()
        )->getUri();

        return sprintf('%s?%s',
            $uri,
            // Symfony 3.x reorders nested parameters - avoid it
            http_build_query($query, '', '&', PHP_QUERY_RFC3986)
        );
    }
}
