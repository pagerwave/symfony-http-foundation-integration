<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony;

use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;
use PagerWave\QueryReader\ArrayQueryReader;
use PagerWave\QueryReader\QueryReaderInterface;
use Symfony\Component\HttpFoundation\Request;

final class RequestQueryReader implements QueryReaderInterface
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getFromRequest(DefinitionInterface $definition): QueryInterface
    {
        return (new ArrayQueryReader($this->request->query->all()))
            ->getFromRequest($definition);
    }
}
