<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony;

use PagerWave\DefinitionInterface;
use PagerWave\Query;
use PagerWave\QueryInterface;
use PagerWave\QueryReader\QueryReaderInterface;
use Symfony\Component\HttpFoundation\RequestStack;

final class RequestStackQueryReader implements QueryReaderInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFromRequest(DefinitionInterface $definition): QueryInterface
    {
        $request = $this->requestStack->getCurrentRequest();

        if (!$request) {
            return new Query();
        }

        return (new RequestQueryReader($request))
            ->getFromRequest($definition);
    }
}
