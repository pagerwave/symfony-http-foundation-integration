# pagerwave/symfony-http-foundation-integration

This package provides integration for [PagerWave][pagerwave] with
Symfony's [HttpFoundation][http-foundation] component.

## Installation

~~~
$ composer require pagerwave/symfony-http-foundation-integration
~~~

## Usage

This library provides query readers and URL generators that operate on `Request`
and `RequestStack` objects from Symfony's HttpFoundation:

~~~php
// Using a RequestStack

use PagerWave\Integration\Symfony\RequestStackQueryReader;
use PagerWave\Integration\Symfony\RequestStackUrlGenerator;

assert($requestStack instanceof \Symfony\Component\HttpFoundation\RequestStack);

$queryReader = new RequestStackQueryReader($requestStack);
$urlGenerator = new RequestStackUrlGenerator($requestStack);
~~~

~~~php
// Using a Request

use PagerWave\Integration\Symfony\RequestQueryReader;
use PagerWave\Integration\Symfony\RequestUrlGenerator;

assert($request instanceof \Symfony\Component\HttpFoundation\Request);

$queryReader = new RequestQueryReader($request);
$urlGenerator = new RequestUrlGenerator($request);
~~~

This should also play nicely with any Symfony-derived frameworks that use either
of these HTTP abstractions, such as Laravel.

Read the [PagerWave documentation][docs] to learn more.

## Licence

This project is released under the Zlib licence.


[docs]: https://gitlab.com/pagerwave/PagerWave/-/tree/1.x/docs
[http-foundation]: https://symfony.com/doc/current/components/http_foundation.html
[pagerwave]: https://gitlab.com/pagerwave/PagerWave
