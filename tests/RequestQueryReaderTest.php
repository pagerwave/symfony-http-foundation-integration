<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony\Tests;

use PagerWave\Exception\IncompleteQueryException;
use PagerWave\Integration\Symfony\RequestQueryReader;
use PagerWave\Integration\Symfony\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \PagerWave\Integration\Symfony\RequestQueryReader
 */
class RequestQueryReaderTest extends TestCase
{
    public function testCanReadCompleteQuery(): void
    {
        $request = new Request([
            'next' => [
                'ranking' => '4',
                'id' => '3',
            ],
        ]);

        $query = (new RequestQueryReader($request))
            ->getFromRequest(new EntityDefinition());

        $this->assertSame('4', $query->get('ranking'));
        $this->assertSame('3', $query->get('id'));
    }

    public function testCanReadMissingQuery(): void
    {
        $query = (new RequestQueryReader(new Request()))
            ->getFromRequest(new EntityDefinition());

        $this->assertFalse($query->isFilled());
    }

    public function testThrowsOnIncompleteQuery(): void
    {
        $request = new Request([
            'next' => [
                'ranking' => '4',
                // missing 'id',
            ],
        ]);

        $this->expectException(IncompleteQueryException::class);

        (new RequestQueryReader($request))
            ->getFromRequest(new EntityDefinition());
    }
}
