<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony\Tests\Fixtures;

trait UrlProviderTrait
{
    public function provideUrls(): iterable
    {
        yield [
            'https://localhost/?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => '4', 'id' => '3'],
            'https://localhost/?next%5Branking%5D=5&next%5Bid%5D=4',
        ];

        yield [
            'http://example.com/foo?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => '4', 'id' => '3'],
            'http://example.com/foo?next%5Branking%5D=5&next%5Bid%5D=4',
        ];
    }
}
