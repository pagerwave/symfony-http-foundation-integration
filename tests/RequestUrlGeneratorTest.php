<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony\Tests;

use PagerWave\Integration\Symfony\RequestUrlGenerator;
use PagerWave\Integration\Symfony\Tests\Fixtures\UrlProviderTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @covers \PagerWave\Integration\Symfony\RequestUrlGenerator
 */
class RequestUrlGeneratorTest extends TestCase
{
    use UrlProviderTrait;

    /**
     * @dataProvider provideUrls
     */
    public function testCanGenerateUrl(
        string $expectedUrl,
        array $params,
        string $currentUrl
    ): void {
        $request = Request::create($currentUrl);
        $generator = new RequestUrlGenerator($request);

        $this->assertSame(
            $expectedUrl,
            $generator->generateUrl('next', $params)
        );
    }
}
