<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony\Tests;

use PagerWave\Exception\UrlGenerationFailedException;
use PagerWave\Integration\Symfony\RequestStackUrlGenerator;
use PagerWave\Integration\Symfony\Tests\Fixtures\UrlProviderTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @covers \PagerWave\Integration\Symfony\RequestStackUrlGenerator
 */
final class RequestStackUrlGeneratorTest extends TestCase
{
    use UrlProviderTrait;

    /**
     * @dataProvider provideUrls
     */
    public function testCanGenerateUrl(
        string $expectedUrl,
        array $params,
        string $currentUrl
    ): void {
        $request = Request::create($currentUrl);

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $generator = new RequestStackUrlGenerator($requestStack);

        $this->assertSame(
            $expectedUrl,
            $generator->generateUrl('next', $params)
        );
    }

    public function testThrowsOnEmptyRequestStack(): void
    {
        $requestStack = new RequestStack();
        $generator = new RequestStackUrlGenerator($requestStack);

        $this->expectException(UrlGenerationFailedException::class);

        $generator->generateUrl('next', ['ranking' => '4', 'id' => '3']);
    }
}
