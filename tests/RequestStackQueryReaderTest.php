<?php

declare(strict_types=1);

namespace PagerWave\Integration\Symfony\Tests;

use PagerWave\Exception\IncompleteQueryException;
use PagerWave\Integration\Symfony\RequestStackQueryReader;
use PagerWave\Integration\Symfony\Tests\Fixtures\EntityDefinition;
use PagerWave\Query;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @covers \PagerWave\Integration\Symfony\RequestStackQueryReader
 */
class RequestStackQueryReaderTest extends TestCase
{
    /**
     * @var RequestStackQueryReader
     */
    private $reader;

    /**
     * @var RequestStack
     */
    private $requestStack;

    protected function setUp(): void
    {
        $this->requestStack = new RequestStack();
        $this->reader = new RequestStackQueryReader($this->requestStack);
    }

    public function testCanReadCompleteQuery(): void
    {
        $this->requestStack->push(new Request([
            'next' => [
                'ranking' => '69',
                'id' => '420',
            ],
        ]));

        $query = $this->reader->getFromRequest(new EntityDefinition());

        $this->assertInstanceOf(Query::class, $query);
        $this->assertTrue($query->isFilled());
        $this->assertSame('69', $query->get('ranking'));
        $this->assertSame('420', $query->get('id'));
    }

    public function testMissingParamsReadsAsMissingQuery(): void
    {
        $this->requestStack->push(new Request());

        $query = $this->reader->getFromRequest(new EntityDefinition());

        $this->assertFalse($query->isFilled());
    }

    public function testEmptyRequestStackReadsAsMissingQuery(): void
    {
        $query = $this->reader->getFromRequest(new EntityDefinition());

        $this->assertFalse($query->isFilled());
    }

    public function testThrowsOnIncompleteQuery(): void
    {
        $this->requestStack->push(new Request([
            'next' => [
                'id' => '420',
            ],
        ]));

        $this->expectException(IncompleteQueryException::class);

        $this->reader->getFromRequest(new EntityDefinition());
    }
}
